import java.util.*;

public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
	  int size = 10;
	  Animal[] arr = new Animal[size];
	  Random rand = new Random();
	  for (int i = 0; i < arr.length; i++) {
		  arr[i] = rand.nextInt(2) == 0 ? Animal.sheep : Animal.goat;
	  }
	  System.out.println("Initial array: " + Arrays.toString(arr));
	  long start = System.currentTimeMillis();
	  reorder(arr);
	  long end = System.currentTimeMillis();
	  int delta = (int) (end - start);
	  System.out.println("Reorder time: " + delta);
	  System.out.println("Reordered array: " + Arrays.toString(arr));
   }
   
   public static void reorder (Animal[] animals) {
	  int goats = 0;
	  for (Animal a: animals) {
		  if (a == Animal.goat) {
			  goats++;
		  }
	  }
	  for (int i = 0; i < animals.length; i++) {
		  animals[i] = i < goats ? Animal.goat : Animal.sheep;
	  }
   }
   
   public static void reorder2 (Animal[] animals) {
	   int l = 0;
	   int r = animals.length - 1;
	   while (l < r) {
		   while ( (l < animals.length) && (animals[l] != Animal.sheep) ) {
			   l++;
		   }
		   while ( (r >= 0) && (animals[r] != Animal.goat) ) {
			   r--;
		   }
		   if (l < r) {
			   Animal temp = animals[r];
			   animals[r] = animals[l];
			   animals[l] = temp;
			   l++;
			   r--;
		   }
	   }
   }
}

